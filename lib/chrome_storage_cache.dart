library chrome_storage_cache;

import 'dart:async';
import 'package:quiver/cache.dart';
import 'package:chrome/chrome_app.dart' as chrome;

class StorageCache<K, V> implements Cache<K, V> {

  final String _prefix;

  // TODO: allow a default, unique prefix
  StorageCache(String prefix) : _prefix = prefix + '-';

  Future<V> get(K key, {Loader<K> ifAbsent}) {
    return chrome.storage.local.get(_full(key)).then((items) {
      if (!items.containsKey(_full(key)) && ifAbsent != null) {
        var valOrFuture = ifAbsent(key);
        if (valOrFuture is Future) {
          return valOrFuture.then((v) {
            set(key, v);
            return v;
          });
        } else {
          set(key, valOrFuture);
          return new Future.value(valOrFuture);
        }
      } else {
        return new Future.value(items[_full(key)]);
      }
    });
  }

  Future set(K key, V value) => chrome.storage.local.set({_full(key):value})
      .whenComplete(() => new Future.value());

  Future invalidate(K key) => chrome.storage.local.remove(_full(key))
      .whenComplete(() => new Future.value());

  String _full(K key) => '$_prefix$key';
}
