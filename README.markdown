# chrome_storage_cache
A `quiver:Cache` implementation backed by `chrome.storage.local`.

## Usage

    Cache<K,V> myCache = new StorageCache('my-prefix');

    myCache.set('favoriteColor','green');

    myCache.get('favoriteColor').then((item) => print(item));
    #=> 'green'

    myCache.get('spoon', ifAbsent: (key) => 'There is no $key.');
    #=> 'There is no spoon.'

    myCache.get('spoon').then((item) => print(item));
    #=> 'There is no spoon.'

## Caveats
* This is a work in progress. Proceed with caution.
* This has only been tested on Chrome Apps, not Chrome Extensions.
* You *must* supply a prefix to the StorageCache constructor.
* **This only works for objects that can be serailzed via the Chrome Storage API.** See below for more information.

### Object serialization

From the Chrome App JavaScript API docs:

> Primitive values such as numbers will serialize as expected. Values with a
> `typeof "object"` and `"function"` will typically serialize to `{}`, with the
> exception of `Array` (serializes as expected), `Date`, and `Regex` (serialize using
> their `String` representation).

The behavior under Dart is identical.

## Further reading
* [Chrome Storage API](https://developer.chrome.com/extensions/storage#type-StorageArea)
* [`quiver:Cache class`](http://www.dartdocs.org/documentation/quiver/0.21.0+1/index.html#quiver/quiver-cache.Cache)

